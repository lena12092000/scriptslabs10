import { Component } from '@angular/core';
import { Sensor } from './shared/models/sensor.model';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  sensors: Sensor[] = [];
  constructor(){
    for(let i = 0; i<10; i++){
      this.sensors.push(new Sensor(i, `Датчик_${i}`));
    //  let sensor: Sensor = new Sensor(i, `Датчик_${i}`)
    }
    console.log(this.sensors)
  }

  onDeleteSensor(idx:number){
    this.sensors.splice(idx, 1);
  }
  id;
  name;
  add(){
    this.sensors.push(new Sensor(this.id, this.name));

  }
}
